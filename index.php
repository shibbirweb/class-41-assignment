<?php
include_once 'database.php';
$query = "SELECT * FROM tbl_user_data";
if (!mysqli_query($link, $query)) {
    die('Error : ' . mysqli_error($link));
} else {
    $result = mysqli_query($link, $query);
}
$allData = array();
while ($row = mysqli_fetch_assoc($result)) {
    $allData[] = $row;
}

//echo "<pre>";
//print_r($allData);
//echo "</pre>";

include_once 'header.php';
?>
<h2 align="center">Student Information</h2>
<?php
if (isset($_SESSION['message'])) {
    ?>
    <h2 align="center"><?php echo $_SESSION['message']; ?></h2>
    <?php
    unset($_SESSION['message']);
}
?>
<table border="1" cellspacing="0" cellpadding="3" align="center">
    <tr align="center">
        <td>Sl No</td>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Gender</td>
        <td>E-mail Address</td>
        <td>Hobby</td>
        <td>Department</td>
        <td>Action</td>
    </tr>
    <?php
    if (count($allData) != 0) {
        $serial = 0;
        foreach ($allData as $data) {
            $serial++;
            ?>
            <tr align="center">
                <td><?php echo $serial; ?></td>
                <td><?php if(!empty($data['first_name'])){echo $data['first_name'];}else{echo "No first name was provided";} ?></td>
                <td><?php if(!empty($data['last_name'])){echo $data['last_name'];}else{echo "No last name was provided";} ?></td>
                <td><?php if(!empty($data['gender'])){echo $data['gender'];}else{echo "No gender was selected";} ?></td>
                <td><?php if(!empty($data['email'])){echo $data['email'];}else{echo "No email was provided";} ?></td>
                <td>
                    <?php
                    $hobbies = unserialize($data['hobby']);
                    if (!empty($hobbies)) {
                            foreach ($hobbies as $hobby) {
                                echo $hobby . ' ';
                            }
                    } else {
                        echo "No hobby was chossen";
                    }
                    ?>
                </td>
                <td><?php if(!empty($data['department'])){echo $data['department'];}else{echo "No department was selected";} ?></td>
                <td>
                    <a href="show.php?id=<?php echo $data['id']; ?>">Show</a> |
                    <a href="edit.php?id=<?php echo $data['id']; ?>">Edit</a> |
                    <a href="delete.php?id=<?php echo $data['id']; ?>" onclick="return confirm('Are you sure to delete this?');">Delete</a>
                </td>
            </tr>
            <?php
        }
    } else {
        ?>
        <tr>
            <td colspan="8" align="center">No data availabel</td>
        </tr>
        <?php
    }
    ?>
</table>
<h3><a href="create.php">Create New Student Information</a></h3>

<?php
include_once 'footer.php';
?>