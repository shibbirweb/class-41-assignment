<?php
include_once 'database.php';

$id = $_GET['id'];
$query = "SELECT * FROM tbl_user_data WHERE id = $id";
if(!mysqli_query($link, $query)){
    die('Error : '.mysqli_error($link));
} else {
    $result = mysqli_query($link, $query);
}
$data = mysqli_fetch_assoc($result);

include_once 'header.php';
?>
<h2 align="center">Individual Student Information</h2>
<table border="1" cellspacing="0" cellpadding="3" align="center">
    <tr>
        <th>Full Name</th>
        <td><?php if(!empty($data['first_name']) || !empty($data['last_name'])){echo $data['first_name'] . ' ' . $data['last_name'];}else{echo "No name is provided";} ?></td>
    </tr>
    <tr>
        <th>Gender</th>
        <td><?php if(!empty($data['gender'])){echo $data['gender'];}else{echo "No gender is selected";} ?></td>
    </tr>
    <tr>
        <th>E-mail Address</th>
        <td><?php if(!empty($data['email'])){echo $data['email'];}else{echo "No email is provied";} ?></td>
    </tr>
    <tr>
        <th>Hobby</th>
        <td>
            <?php
            $hobbies = unserialize($data['hobby']);
            if(!empty($hobbies)){
                foreach ($hobbies as $hobby) {
                echo $hobby . ' ';
                }
            }else{
                echo "No hobby is chossen";
            }
            
            ?>
        </td>
    </tr>
    <tr>
        <th>Department</th>
        <td><?php if(!empty($data['department'])){echo $data['department'];}else{echo "No department is selected";} ?></td>
    </tr>
    <tr>
        <th>Action</th>
        <td>
            <a href="edit.php?id=<?php echo $id; ?>">Edit</a> |
            <a href="delete.php?id=<?php echo $id; ?>" onclick="return confirm('Are sure to delete this?');">Delete</a>
        </td>
    </tr>
</table>
<h4><a href="create.php">Create</a></h4>

<h4><a href="index.php">Back to list</a></h4>


<?php
include_once 'footer.php';
?>

