<?php
include_once 'header.php';
?>

<form action="add.php" method="POST">
    <fieldset>
        <legend>New Student Information</legend>
        <p>
            <label for="first_name">First Name</label> :
            <input type="text" name="first_name" placeholder="Enter first name" id="first_name" />
        </p>
        <p>
            <label for="last_name">Last Name </label> : 
            <input type="text" name="last_name" placeholder="Enter last name" id="last_name" />
        </p>
        <p>
            <label>Gender</label> : 
            <input id="male" type="radio" name="gender" value="Male" />
            <label for="male">Male</label>
            <input id="female" type="radio" name="gender" value="Female" />
            <label for="female">Female</label>
        </p>
        <p>
            <label for="email">E-mail address</label> : 
            <input type="email" id="email" name="email" placeholder="Enter email address" />
        </p>
        <p>
            <label>Hobby </label>: 
            <input id="coding" type="checkbox" name="hobby[]" value="Coding" />
            <label for="coding">Coding</label>
            <input id="chatting" type="checkbox" name="hobby[]" value="Chatting" />
            <label for="chatting">Chatting</label>
            <input id="blogging" type="checkbox" name="hobby[]" value="Blogging" />
            <label for="blogging">Blogging</label>
        </p>
        <p>
            <label>Department </label> :
            <select name="department">
                <option value="">--Select Department--</option>
                <option value="Mathematics">Mathematics</option>
                <option value="Physics">Physics</option>
                <option value="Bangla">Bangla</option>
            </select>
        </p>
        <p>
            <input type="submit" name="submit" value="Save Student Info" />
            <input type="reset"  value="Reset" />
        </p>
    </fieldset>
</form>
<h4><a href="index.php">Back to list</a></h4>
<?php
include_once 'footer.php';
?>