<?php
include_once 'database.php';
$id = $_GET['id'];
$query = "SELECT * FROM tbl_user_data WHERE id = $id";

if (!mysqli_query($link, $query)) {
    die('Error : ' . mysqli_error($link));
} else {
    $result = mysqli_query($link, $query);
}
$data = mysqli_fetch_assoc($result);
$hobbies = unserialize($data['hobby']);
include_once 'header.php';
?>
<form action="store.php" method="POST">
    <input type="hidden" name="id" value="<?php echo $data['id']; ?>" />
    <fieldset>
        <legend>Edit Student Information</legend>
        <p>
            <label for="first_name">First Name</label> :
            <input id="first_name"  type="text" name="first_name" value="<?php
            if (!empty($data['first_name'])) {
                echo $data['first_name'];
            }
            ?>"/>
        </p>
        <p>
            <label for="last_name">Last Name </label> : 
            <input  id="last_name" type="text" name="last_name" value="<?php
            if (!empty($data['last_name'])) {
                echo $data['last_name'];
            }
            ?>" />
        </p>
        <p>
            <label>Gender</label> : 
            <input type="radio"
            <?php
            if ($data['gender'] == 'Male') {
                echo "checked='checked'";
            }
            ?> 
                   name="gender" value="Male" id="male"  />
            <label for="male">Male</label>
            <input type="radio"
            <?php
            if ($data['gender'] == 'Female') {
                echo "checked='checked'";
            }
            ?> 
                   name="gender" value="Female" id="female" />
            <label for="female">Female</label>
        </p>
        <p>
            <label for="email">E-mail address</label> : 
            <input id="email" type="email" name="email" value="<?php
            if (!empty($data['email'])) {
                echo $data['email'];
            }
            ?>" />
        </p>
        <p>
            <label>Hobby </label>: 
            <input type="checkbox" 
            <?php
            if (!empty($hobbies)) {
                if (in_array('Coding', $hobbies)) {
                    echo "Checked= 'checked'";
                }
            }
            ?> 
                   name="hobby[]" value="Coding"  id="coding"/>
            <label for="coding">Coding</label>
            <input type="checkbox" 
            <?php
            if (!empty($hobbies)) {
                if (in_array('Chatting', $hobbies)) {
                    echo "Checked= 'checked'";
                }
            }
            ?> 
                   name="hobby[]" value="Chatting" id="chatting" />
            <label for="chatting">Chatting</label>
            <input type="checkbox" 
            <?php
            if (!empty($hobbies)) {
                if (in_array('Blogging', $hobbies)) {
                    echo "Checked= 'checked'";
                }
            }
            ?> 
                   name="hobby[]" value="Blogging" id="blogging" />
            <label for="blogging">Blogging</label>
        </p>
        <p>
             <label>Department </label> :
            <select name="department">
                <option value="">--Select Department--</option>
                <option 
                <?php
                if (!empty($data['department'])) {
                    if ($data['department'] == 'Mathematics') {
                        echo "selected='selected'";
                    }
                }
                ?>
                    value="Mathematics">Mathematics</option>
                <option 
                <?php
                if (!empty($data['department'])) {
                    if ($data['department'] == 'Physics') {
                        echo "selected='selected'";
                    }
                }
                ?>
                    value="Physics">Physics</option>
                <option 
                <?php
                if (!empty($data['department'])) {
                    if ($data['department'] == 'Bangla') {
                        echo "selected='selected'";
                    }
                }
                ?>
                    value="Bangla">Bangla</option>
            </select>
        </p>
        <p>
            <input type="submit" name="update" value="Update Student Info" />
        </p>
    </fieldset>
</form>
<h4><a href="index.php">Back to list</a></h4>
<?php
include_once 'footer.php';
?>